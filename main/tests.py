from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from .views import *
from .forms import *
from django.apps import apps
from django.http import HttpRequest

# Create your tests here.
class Story9Test(TestCase):

    def test_apakah_url_ada(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_fungsi_index(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_fungsi_login(self):
        found = resolve('/login/')
        self.assertEqual(found.func, login)

    def test_fungsi_logout(self):
        found = resolve('/logout/')
        self.assertEqual(found.func, logout)
        
    def test_fungsi_signup(self):
        found = resolve('/signup/')
        self.assertEqual(found.func, signup)

    def test_apakah_landingpage_ada_htmlnya(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'story9.html')

    def test_apakah_loginpage_ada_url(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_loginpage_ada_htmlnya(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login.html')
    
    def test_page_template_logout(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code,302)
    
    def test_apakah_url_story9_ada(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response, 'story9.html')

    def test_apakah_signup_page_ada_urlnya(self):
        response = Client().get('/signup/')
        self.assertEqual(response.status_code, 200)

    def test_form_post(self):
        signUp_form = SignUpForm(data={'email':'abcd@gmail.com', 'full_name': 'abcd'})
        self.assertFalse(signUp_form.is_valid())
        self.assertEqual(signUp_form.cleaned_data['email'],"abcd@gmail.com")
        self.assertEqual(signUp_form.cleaned_data['full_name'],"abcd") 